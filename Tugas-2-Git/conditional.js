var nama  = ""
var peran = ""

if (nama == peran ) {
    // Output untuk Input nama = '' dan peran = ''
    console.log("Nama Harus diisi!")
} else {
    console.log("Pilih Peranmu untuk memulai game")
}

var nama  = "john"
var peran = ""

if (nama != peran) {
    //Output untuk Input nama = 'John' dan peran = ''
    console.log("Halo John, Pilih peranmu untuk memulai game!")
} else {
    console.log("Nama Harus diisi!")
}


var nama  = "jane"
var peran = "penyihir"

if (nama != peran) {
    //Output untuk Input nama = 'Jane' dan peran 'Penyihir'
    console.log("Selamat datang di Dunia Warewolf, Jane")
    console.log("Halo Penyihir Jane, kamu dapat melihat siapa yang menjadi werewolf!")
} else {
    console.log("Pilih Peranmu untuk memulai game")
}

var nama  = "jenita"
var peran = "guard"

if (nama != peran) {
    //Output untuk Input nama = 'Jenita' dan peran 'Guard'
    console.log("Selamat datang di Dunia Werewolf, Jenita")
    console.log("Halo Guard Jenita, kamu akan membantu melindungi temanmu dari serangan werewolf.")
} else {
    console.log("Nama Harus Diisi")
}

var nama  = "junaedi"
var peran = "werewolf"

if (nama != peran) {
    //Output untuk Input nama = 'Junaedi' dan peran 'Werewolf'
    console.log("Selamat datang di Dunia Werewolf, Junaedi")
    console.log("Halo Werewolf Junaedi, Kamu akan memakan mangsa setiap malam!")
} else {
    console.log("Nama Harus diisi")
}


var a = "============================================================================"
console.log(a)

var hari  = 21;

switch(hari) {
    case 1:   { console.log('1 Januari 1945'); break; }
    case 2:   { console.log('2 Januari 1945'); break; }
    case 3:   { console.log('3 Januari 1945'); break; }
    case 21:   { console.log('21 Januari 1945'); break; }
    default:  { console.log('Tidak terjadi apa-apa'); }}
